const passport = require('passport');
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

module.exports = (app, db) => {
  app.post('/create-comment/:post_id', passport.authenticate('jwt', { session: false }),
    function (req, res) {
      // lab 1
      db.comment.create({
        message: req.body.message,
        post_id: req.params.post_id,
        user_id: req.user.id
      })
        .then(result => {
          res.json(result)
        })
        .catch(ex => {
          res.status(400).json({ errorMessage: ex })
        })
    }
  )

  app.put('/update-comment/:id', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // lab 2
      // @TODO: ยังไม่เสร็จ
      const targetComment = await db.comment.findOne({
        where: { id: req.params.comment_id }
      })

      if (!targetComment) {
        res.status(404).send({errorMessage: "Comment not found"})
      } else if (targetComment.user_id !== req.user.id) {
        res.status(401).send({errorMessage: 'Unauthorized'})
      } else {

      }
      // try {
      //   const user = await db.user.findOne({ where: { id: req.query.user_id } })
      //   if (!user) throw await new Error('User not found')
      //   const result = await db.comment.update(
      //     { message: req.body.message },
      //     { where: { id: req.params.id, user_id: req.query.user_id } }
      //   )
      //   res.json(result)
      // } catch (ex) {
      //   res.status(400).json({ errorMessage: ex })
      // }
    })

  app.delete('/delete-comment/:id', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // Lab 3
      // await db.comment.findOne()
    })
}
