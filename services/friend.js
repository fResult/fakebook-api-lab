const passport = require('passport');
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

module.exports = (app, db) => {
  class elevator {

  }
  app.post('/friend-request-to/:id', passport.authenticate('jwt', { session: false }),
    function (req, res) {
      // Lab 1
      db.friend.findOne({
        where: {
          [Op.or]: [
            { request_to_id: req.params.id, request_from_id: req.user.id },
            { request_to_id: req.user.id, request_from_id: req.params.id }
          ]
        }
      })
        .then(targetFriendReq => {
          if (targetFriendReq === null) {
            db.friend.create({
              request_from_id: req.user.id,
              request_to_id: req.params.id,
              status: 'request'
            })
              .then(result => {
                res.status(201).json({ message: result })
              })
              .catch(error => {
                res.status(400).json({ errorMessage: error })
              })
          } else {
            res.status(400).json({ errorMessage: 'already requested to this user' })
          }
        })
        .catch(error => {
          console.log(error)
          res.status(400).json({ errorMessage: error })
        })
    }
  )

  app.get('/request-list', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // Lab 2
      try {
        console.log(req.user.id)
        const requestList = await db.friend.findAll({
          where: { request_to_id: req.user.id },
          attributes: ['request_from_id']
        })

        const requestFromIds = requestList.map(request => request.request_from_id)
        console.log('Hello', requestFromIds)

        const result = await db.user.findAll({
          where: {
            id: { [Op.in]: requestFromIds }
          },
          attributes: ['id', 'name', 'profile_img_url']
        })
        res.json({ message: result })
      } catch (ex) {
        res.status(400).json({ errorMessage: ex })
      }

    });

  app.patch('/accept-friend-request/:id', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // Lab 3
      try {
        const targetFriend = await db.friend.findOne({
          where: { request_from_id: req.params.id, request_to_id: req.user.id, status: 'request' },
        })
        if (targetFriend !== null) {
          await targetFriend.update({ status: 'friend' })
          res.status(200).json({ message: 'You accept friend' })
        } else {
          res.status(404).json({ errorMessage: 'Friend request not found' })
        }
      } catch (ex) {
        console.error(ex)
        res.status(404).json({ errorMessage: 'ex' })
      }
    }
  )

  app.delete('/deny-friend-request/:id', passport.authenticate('jwt', { session: false }),
    function (req, res) {
      // Lab 4
      db.friend.findOne({ where: { request_to_id: req.user.id, request_from_id: req.params.id, status: 'request' } })
        .then(targetRequest => {
          if (targetRequest !== null) {
            targetRequest.destroy()
            res.send({ message: `Already denied` })
          } else {
            res.status(400).send({ errorMessage: 'Request not found' })
          }
        })
        .catch(error => {
          console.log(error)
          res.status(400).send({ errorMessage: error.message })
        })
    }
  )

  app.delete('/delete-friend/:id', passport.authenticate('jwt', { session: false }),
    function (req, res) {
      // Lab 5
      db.friend.findOne({
        where: {
          [Op.or]: [
            { request_to_id: req.user.id, request_from_id: req.params.id, status: 'friend' },
            { request_from_id: req.user.id, request_to_id: req.params.id, status: 'friend' }
          ]
        }
      })
        .then(targetFriend => {
          if (targetFriend !== null) {
            targetFriend.destroy()
            res.send({ message: `Already deleted friend` })
          } else {
            res.status(400).send({ errorMessage: 'Friend not found' })
          }
        })
        .catch(error => {
          console.log(error)
          res.status(400).send({ errorMessage: error.message })
        })
    }
  )

  app.get('/friend-list', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // Lab 6
      const requestFromIds = await db.friend.findAll({
        where: { status: 'friend', request_to_id: req.user.id },
        attributes: [['request_from_id', 'id']]
      });
      const requestToIds = await db.friend.findAll({
        where: { status: 'friend', request_from_id: req.user.id },
        attributes: [['request_to_id', 'id']]
      })
      const requestFromIdsArr = requestFromIds.map(request => request.id)
      const requestToIdsArr = requestToIds.map(request => request.id)
      const friendList = await db.user.findAll({
        where: { id: { [Op.in]: requestFromIdsArr.concat(requestToIdsArr) } },
        attributes: ['id', 'name', 'profile_img_url']
      })
      res.status(200).send(friendList)
    }
  );
}