const passport = require('passport');
const Sequelize = require('sequelize')
const Op = Sequelize.Op;

module.exports = (app, db) => {
  app.post('/create-post', passport.authenticate('jwt', { session: false }),
    // lab 1
    function (req, res) {
      db.post.create({
        message: req.body.message,
        image_url: req.body.image_url,
        user_id: req.user_id
      })
        .then(result => {
          res.status(201).send(result)
        })
        .catch(err => {
          res.status(400).send(err)
        })
    }
  )

  app.put('/update-post/:id', passport.authenticate('jwt', { session: false }),
    // lab 2
    async function (req, res) {
      try {
        const targetPost = await db.post.findOne({ where: { id: req.params.id } })
        if (req.user.id === targetPost.user_id) {
          try {
            const postUpdateResult = await targetPost.update({
              message: req.body.message,
              image_url: req.body.image_url
            })
            console.log(postUpdateResult)
            res.status(200).json({ message: postUpdateResult })
          } catch (ex) {
            console.log('❌ 400', ex)
            res.status(400).send({ errorMessage: ex })
          }
        } else {
          res.status(403).json({ errorMessage: 'not permission' })
        }
      } catch (ex) {
        console.log('❌ 204', ex)
        res.status(204).send({ errorMessage: 'no content' })
      }
    }
  )

  app.delete('/delete-post/:id', passport.authenticate('jwt', { session: false }),
    // lab 3
    async function (req, res) {
      try {
        const targetPost = await db.post.destroy({ where: { id: req.params.id, user_id: req.user.id } })
        if (!targetPost) res.status(404).send('Not found post or Unauthorize')
        const removePostResult = targetPost.destroy()
        res.json({ message: removePostResult })
      } catch (ex) {
        res.status(400).json({ errMessage: ex })
      }
    })

  app.get('/my-posts', passport.authenticate('jwt', { session: false }),
    // lab 4
    async function (req, res) {
      try {
        const result = await db.post.findAll({
          include: [
            {
              model: db.user,
              attributes: ['id', 'name', 'profile_img_url']
            },
            {
              model: db.comment,
              include: [{
                model: db.user,
                attributes: ['id', 'name', 'profile_img_url']
              }]
            },
          ]
        })
        res.json(result)
      } catch (ex) {
        res.status(400).json({ errMessage: ex })
      }
    }
  );

  app.get('/feed', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // Lab 5
      const requestFromIds = await db.friend.findAll({
        where: { status: 'friend', request_to_id: req.user.id },
        attributes: [['request_from_id', 'id']]
      });
      const requestToIds = await db.friend.findAll({
        where: { status: 'friend', request_from_id: req.user.id },
        attributes: [['request_to_id', 'id']]
      })
      const requestFromIdsArr = requestFromIds.map(request => request.id)
      const requestToIdsArr = requestToIds.map(request => request.id)
      const allIds = requestFromIdsArr.concat(requestToIdsArr).concat([req.user.id])
      db.post.findAll({
        where: {
          user_id: { [Op.in]: allIds }
        },
        include: [
          {
            model: db.user,
            attributes: ['id', 'name', 'profile_img_url']
          },
          {
            model: db.comment,
            include: [{ model: db.user, attributes: ['id', 'name', 'profile_img_url'] }]
          }
        ]
      })
      res.status(200).send(allFeedPost)
    }
  );
}