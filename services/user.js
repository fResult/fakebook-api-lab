const jwt = require('jsonwebtoken');
const passport = require('passport');
const config = require('../config/passport/passport');
const bcrypt = require('bcryptjs')

const Sequelize = require('sequelize')
const Op = Sequelize.Op;

module.exports = (app, db) => {
  app.post('/registerUser', (req, res, next) => {
    passport.authenticate('register', (err, user, info) => {
      if (err) {
        console.error(err);
      }
      if (info !== undefined) {
        console.error(info.message);
        res.status(403).send(info.message);
      } else {
        const data = {
          name: req.body.name,
          username: user.username,
          profile_img_url: req.body.profile_img_url,
          role: "user"
        };
        console.log(data);
        db.user.findOne({
          where: {
            username: data.username,
          },
        }).then(user => {
          user
            .update({
              name: data.name,
              email: data.email,
              profile_img_url: data.profile_img_url,
              role: data.role
            })
            .then(() => {
              console.log('user created in db');
              res.status(200).send({ message: 'user created' });
            });
        })
          .catch(err => {
            console.log(err)
          })
      }
    })(req, res, next);
  });

  app.post('/loginUser', (req, res, next) => {
    passport.authenticate('login', (err, users, info) => {
      if (err) {
        console.error(`error ${err}`);
      }
      if (info !== undefined) {
        console.error(info.message);
        if (info.message === 'bad username') {
          res.status(401).send(info.message);
        } else {
          res.status(403).send(info.message);
        }
      } else {
        db.user.findOne({
          where: {
            username: req.body.username,
          },
        }).then(user => {
          const token = jwt.sign({ id: user.id, role: user.role }, config.jwtOptions.secretOrKey, {
            expiresIn: 3600,
          });
          res.status(200).send({
            auth: true,
            token,
            message: 'user found & logged in',
          });
        });
      }
    })(req, res, next);
  });

  app.patch('/change-password', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // Lab 1
      let targetUser = await db.user.findOne({ where: { id: req.user.id } })
      if (!targetUser) {
        res.status(404).send({ message: 'user not found' })
      } else {
        let salt = bcrypt.genSaltSync(config.BCRYPT_SALT_ROUNDS)
        let newHashedPassword = bcrypt.hashSync(req.body.newPassword, salt)
        bcrypt.compare(req.body.oldPassword, req.user.password, (err, response) => {
          console.log({ response })
          if (!response) {
            res.status(401).send({errorMessage: 'Your old password is wrong'})
          } else {
            targetUser.update({
              password: newHashedPassword
            })
            res.send({message: 'Your password is changed'})
          }
        })
      }
    }
  )

  async function getUserInfo(user_id) {
    const user = await db.user.findOne({
      attributes: ['id', 'name'],
      where: { id: user_id },
      raw: true
    })
    return user
  }

  app.get('/user/:id', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
      // Lab 2
      const yourId = req.user.id
      const friendId = parseInt(req.params.id)
      const requestFromUser = await db.friend.findOne({
        attributes: [['request_from_id', 'id'], 'status'],
        where: { request_from_id: friendId, request_to_id: yourId },
        raw: true
      })

      const requestToUser = await db.friend.findOne({
        attributes: [['request_to_id', 'id'], 'status'],
        where: { request_from_id: yourId, request_to_id: friendId },
        raw: true
      })

      if (requestFromUser && requestFromUser.id === friendId && requestFromUser.status === 'request') {
        const user = await getUserInfo(requestFromUser.id)
        res.send({ ...user, statusName: 'รอคำตอบรับจากคุณ' })
      } else if (requestToUser && requestToUser.id === friendId && requestToUser.status === 'request') {
        const user = await getUserInfo(requestToUser.id)
        res.send({ ...user, statusName: 'ขอเป็นเพื่อนแล้ว' })
      } else if (requestFromUser && requestFromUser.id === friendId && requestFromUser.status === 'friend') {
        const user = await getUserInfo(requestFromUser.id)
        res.send({ ...user, statusName: 'เพื่อน' })
      } else if (requestToUser && requestToUser.id === friendId && requestToUser.status === 'friend') {
        const user = await getUserInfo(requestToUser.id)
        res.send({ ...user, statusName: 'เพื่อน' })
      } else {
        const targetUser = await db.user.findOne({ attributes: ['id', 'name'], raw: true, where: { id: friendId } })
        if (!targetUser) {
          res.status(404).send({ errorMessage: 'User not found' })
        } else {
          console.log({ targetUser })
          res.send({ ...targetUser, statusName: 'ขอเเป็นเพื่อน' })
        }
      }
    })
}
